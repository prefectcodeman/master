package com.ming.consumemq.processor;

import com.aliyun.openservices.ons.api.Action;
import com.aliyun.openservices.ons.api.ConsumeContext;
import com.aliyun.openservices.ons.api.Message;
import com.aliyun.openservices.ons.api.MessageListener;

/**
 * @Classname DemoListener
 * @Description TODO
 * @Date 2021/1/13 11:01
 * @Created by yanming.fu
 */
//@Component
public class DemoListener implements MessageListener {

    @Override
    public Action consume(Message message, ConsumeContext context) {
        return null;
    }
}
