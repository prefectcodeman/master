package com.ming.mq.common.enums;

import com.ming.mq.common.constant.TagConstant;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.List;

/**
 * @Classname MqParamMapper
 * @Description TODO
 * @Date 2021/1/13 11:28
 * @Created by yanming.fu
 */
@Slf4j
public enum MqParamMapper {

    USER("ming-user", "group_user", "TOPIC-user", TagConstant.USER_TAG_NAME, "userMqController"),

    SYSTEM("ming-system", "group_system", "TOPIC-system", TagConstant.SYSTEMTAG_NAME, "systemMqController");

    private final String applicationName;
    private final String topic;
    private final String tag;
    private final String groupId;
    private final String beanName;

    MqParamMapper(String applicationName, String groupId, String topic, String tag, String beanName) {
        this.applicationName = applicationName;
        this.topic = topic;
        this.tag = tag;
        this.groupId = groupId;
        this.beanName = beanName;
    }

    public static List<ConsumeEntity> getAddList(String applicationName, String groupId) {
        ArrayList<ConsumeEntity> list = new ArrayList<>();
        for (MqParamMapper value : MqParamMapper.values()) {
            if (value.applicationName.equals(applicationName) &&  value.groupId.equals(groupId)) {
                list.add(new ConsumeEntity(value.applicationName, value.topic, value.tag, value.groupId, value.beanName));
            }
        }
        if (list.isEmpty()) {
            log.error("封装rocketMQ消费者对象过程中,出现异常->根据以下参数[{},{},{}]未能拿到任何一条符合条件的枚举配置", applicationName, groupId);
        }
        return list;
    }

    public String getApplicationName() {
        return applicationName;
    }

    public String getTopic() {
        return topic;
    }

    public String getTag() {
        return tag;
    }

    public String getGroupId() {
        return groupId;
    }

    public String getBeanName() {
        return beanName;
    }

    public static MqParamMapper getProperty(String prefix) {
        return MqParamMapper.valueOf(prefix.toUpperCase());
    }

}
