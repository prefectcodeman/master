package com.ming.producemq.config;

import com.aliyun.openservices.ons.api.PropertyKeyConst;
import com.aliyun.openservices.ons.api.bean.ProducerBean;
import com.ming.common.util.ApplicationContextUtil;
import com.ming.mq.common.config.MingMqConfig;
import com.ming.mq.common.enums.MqParamMapper;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Properties;

/**
 * @Classname SystemMqConfig
 * @Description TODO
 * @Date 2021/1/13 16:18
 * @Created by yanming.fu
 */
@Slf4j
@Configuration
@Data
@ConditionalOnExpression("'ming-system'.equals('${spring.application.name}')")
public class SystemMqConfig {

    private static final String GROUP_ID = MqParamMapper.SYSTEM.getGroupId();

    public Properties getMqProperty() {
        MingMqConfig config = ApplicationContextUtil.getBean(MingMqConfig.class);
        Properties properties = config.getMqProperty();
        properties.setProperty(PropertyKeyConst.GROUP_ID, GROUP_ID);
        return properties;
    }


    @Bean(initMethod = "start", destroyMethod = "shutdown", name = "userProducer")
    public ProducerBean producerBean() {
        ProducerBean producerBean = new ProducerBean();
        producerBean.setProperties(getMqProperty());
        log.info("初始化MQ [UserMqConfig] 生产者:{}", producerBean.getProperties());
        return producerBean;
    }
}
