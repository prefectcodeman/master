package com.ming.user.config;

import com.ming.user.constant.BasicAuthConstant;
import org.apache.commons.codec.binary.Base64;

import java.nio.charset.Charset;

/**
 * @Classname AuthHeadConfig
 * @Description 构造Basic Auth认证头信息
 * @Date 2021/2/3 16:15
 * @Created by yanming.fu
 */
public class AuthHeadConfig {

    /**
     * 构造Basic Auth认证头信息
     *
     * @return
     */
    public static String getHeader() {
        String auth = BasicAuthConstant.APP_KEY + ":" + BasicAuthConstant.SECRET_KEY;
        byte[] encodedAuth = Base64.encodeBase64(auth.getBytes(Charset.forName("US-ASCII")));
        String authHeader = "Basic " + new String(encodedAuth);
        return authHeader;
    }
}
