package com.ming.user.constant;

/**
 * @Classname BasicAuthConstant
 * @Description Basic认证的常量
 * @Date 2021/2/3 16:17
 * @Created by yanming.fu
 */
public class BasicAuthConstant {

    public static final String APP_KEY="ming-auth-api";//企业KEY

    public static final String SECRET_KEY="asdf";  //企业密钥
}
