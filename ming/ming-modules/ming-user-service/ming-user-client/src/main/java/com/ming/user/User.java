package com.ming.user;

import lombok.Builder;
import lombok.Data;

import javax.persistence.*;

/**
 * @Classname UserApplication
 * @Description 用户实体类
 * @Date 2021/1/4 17:47
 * @Created by yanming.fu
 */
@Data
@Builder
@Table(name="sys_user")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY,generator = "JDBC")
    private String id;
    @Column
    private String name;
    @Column
    private Integer age;


}
