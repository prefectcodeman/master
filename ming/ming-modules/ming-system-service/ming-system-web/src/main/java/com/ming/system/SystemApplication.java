package com.ming.system;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import tk.mybatis.spring.annotation.MapperScan;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @Classname UserApplication
 * @Description TODO
 * @Date 2021/1/5 10:42
 * @Created by yanming.fu
 */
@SpringBootApplication
@EnableDiscoveryClient
@MapperScan("com.ming.system.mapper")
public class SystemApplication  implements CommandLineRunner {

    private static final Logger logger = LoggerFactory.getLogger(SystemApplication.class);

    @Value("${ming.info}")
    private String envs;

    public static void main(String[] args) {
        SpringApplication.run(SystemApplication.class, args);
        logger.info("========================【系统服务】启动完毕========================");
    }


    @Override
    public void run(String... args) throws Exception {
        logger.info("========================【{}】========================",envs);
    }
}
