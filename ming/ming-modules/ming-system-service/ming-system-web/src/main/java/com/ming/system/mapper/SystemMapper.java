package com.ming.system.mapper;

import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;
import com.ming.system.System;

/**
 * @Classname SystemMapper
 * @Description TODO
 * @Date 2021/1/5 13:59
 * @Created by yanming.fu
 */
@Repository
public interface SystemMapper extends Mapper<System> {



}
