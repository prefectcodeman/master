package com.ming.gateway.handler;

import com.alibaba.csp.sentinel.adapter.gateway.sc.callback.BlockRequestHandler;
import com.alibaba.csp.sentinel.adapter.gateway.sc.callback.GatewayCallbackManager;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.server.ServerResponse;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

/**
 * @Classname SentinelGatewayConfig
 * @Description TODO
 * @Date 2021/1/11 15:35
 * @Created by yanming.fu
 */
@Configuration
public class SentinelGatewayHandler {

    public SentinelGatewayHandler(){
        GatewayCallbackManager.setBlockHandler(new BlockRequestHandler() {
            @Override
            public Mono<ServerResponse> handleRequest(ServerWebExchange serverWebExchange, Throwable throwable) {
                String errorJson="网关流控实施了限流";
                Mono<ServerResponse>body=ServerResponse.ok().body(Mono.just(errorJson),String.class);
                return  body;
            }
        });
    }
}
