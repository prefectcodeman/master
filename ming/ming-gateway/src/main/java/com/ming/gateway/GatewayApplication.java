package com.ming.gateway;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import springfox.documentation.oas.annotations.EnableOpenApi;

/**
 * @Classname gateway
 * @Description TODO
 * @Date 2021/1/6 14:33
 * @Created by yanming.fu
 */
@SpringBootApplication
@EnableDiscoveryClient
@EnableOpenApi
public class GatewayApplication   implements CommandLineRunner {
    private static final Logger logger = LoggerFactory.getLogger(GatewayApplication.class);

    @Value("${ming.info}")
    private String envs;

    public static void main(String[] args) {
        SpringApplication.run(GatewayApplication.class, args);
        String msg = String.format("%s启动完毕", "【网关服务】");
        System.out.println(msg);
        logger.info("========================【网关服务】启动完毕========================");
    }

    @Override
    public void run(String... args) throws Exception {
        logger.info("========================【{}】========================",envs);
    }

}
