package com.ming.authorization.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Classname IndexController
 * @Description 健康检查类
 * @Date 2021/2/2 15:25
 * @Created by yanming.fu
 */
@RestController
@Slf4j
public class IndexController {


    /**
     * @Description 主请求
     * @return java.lang.String
     * @date 2021/2/2 15:25
     * @auther yanming.fu
     */
    @GetMapping("/")
    public String index(){
        return "鉴权服务正在运行...";
    }

}
